package com.twuc.webApp.repository;

import com.twuc.webApp.domain.composite.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
}
