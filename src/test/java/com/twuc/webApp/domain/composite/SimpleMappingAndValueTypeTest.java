package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.repository.CompanyProfileRepository;
import com.twuc.webApp.repository.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SimpleMappingAndValueTypeTest extends JpaTestBase {

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    public void should_successfully_when_save_and_find_a_company_profile_entity() {
        ClosureValue<Long> closureValue = new ClosureValue<>();

        flushAndClear(em -> {
            CompanyProfile savedCompanyProfile = companyProfileRepository.save(
                    new CompanyProfile("Xian", "south road of unity"));
            closureValue.setValue(savedCompanyProfile.getId());
            assertNotNull(savedCompanyProfile);
        });
        flush(em -> {
            Optional<CompanyProfile> optionalCompanyProfile = companyProfileRepository
                    .findById(closureValue.getValue());
            if (optionalCompanyProfile.isPresent()) {
                CompanyProfile companyProfile = optionalCompanyProfile.get();
                assertEquals("Xian", companyProfile.getCity());
                assertEquals("south road of unity", companyProfile.getStreet());
            }
        });
    }

    @Test
    public void should_successfully_when_save_and_find_a_user_profile_entity() {
        ClosureValue<Long> closureValue = new ClosureValue<>();

        flushAndClear(em -> {
            UserProfile savedUserProfile = userProfileRepository.save(
                    new UserProfile("Xian", "south road of unity"));
            closureValue.setValue(savedUserProfile.getId());
            assertNotNull(savedUserProfile);
        });
        flush(em -> {
            Optional<UserProfile> optionalUserProfile = userProfileRepository
                    .findById(closureValue.getValue());
            if (optionalUserProfile.isPresent()) {
                UserProfile userProfile = optionalUserProfile.get();
                assertEquals("Xian", userProfile.getAddressCity());
                assertEquals("south road of unity", userProfile.getAddressStreet());
            }
        });
    }
}
